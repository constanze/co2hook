package com.hollyhook.co2hook

import android.Manifest
import android.app.ListActivity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothAdapter.LeScanCallback
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentManager
import com.hollyhook.co2hook.CO2Service.Companion.CO2_ACTION_CLOSE_SERVICE
import com.hollyhook.co2hook.CO2Service.Companion.CO2_ACTION_START_SERVICE
import java.util.*


class CO2Activity : ListActivity(),
    SharedPreferences.OnSharedPreferenceChangeListener
{
    private val TAG = "com.hollyhook.CO2Act"

    /** Preferences saving, for threshold setting etc */
    internal var sharedPref: SharedPreferences? = null

    private var scanning = false
    private var handler: Handler? = null
    private var leDeviceListAdapter: LeDeviceListAdapter? = null
    private var bluetoothAdapter: BluetoothAdapter? = null

    private val REQUEST_ENABLE_BT = 1

    // Stops scanning after 10 seconds.
    private val SCAN_PERIOD: Long = 10000


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionBar.setTitle(R.string.title_devices)
        sharedPref = getSharedPreferences(getString(R.string.preference_file_name), Context.MODE_PRIVATE)
        sharedPref?.registerOnSharedPreferenceChangeListener(this)
        handler = Handler()

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show()
            finish()
        }

        // since android 6, permission must be granted at runtime
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION
                //, Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            1001
        )

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        val bluetoothManager = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter

        // Checks if Bluetooth is supported on the device.
        if (bluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show()
            finish()
            return
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        if (!scanning) {
            menu.findItem(R.id.menu_stop).isVisible = false
            menu.findItem(R.id.menu_scan).isVisible = true
        } else {
            menu.findItem(R.id.menu_stop).isVisible = true
            menu.findItem(R.id.menu_scan).isVisible = false
        }
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        handler?.removeCallbacksAndMessages(null);
        when (item.itemId) {
            R.id.menu_scan -> {
                leDeviceListAdapter?.clear()
                scanLeDevice(true)
            }
            R.id.menu_stop -> scanLeDevice(false)
            R.id.action_help -> {
                // start activity with help text
                val i = Intent(applicationContext!!, HelpActivity().javaClass)
                this.startActivity(i)
                true
            }
            R.id.action_setting -> {
                val dialog = SettingsDialog()
                val f = this.fragmentManager
                dialog.show(f, "SettingsDialog")
                true
            }

        }
        return true
    }

    override fun onResume() {
        super.onResume()

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!bluetoothAdapter?.isEnabled!!) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(
                enableBtIntent,
                REQUEST_ENABLE_BT
            )
        }

        // Initializes list view adapter.
        leDeviceListAdapter = LeDeviceListAdapter(this)
        listAdapter = leDeviceListAdapter

        scanLeDevice(true)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_CANCELED) {
            finish()
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onPause() {
        super.onPause()
        handler?.removeCallbacksAndMessages(null);
        scanLeDevice(false)
        leDeviceListAdapter?.clear()
    }



    override fun onListItemClick(l: ListView?, v: View?, position: Int, id: Long) {
        val device: BluetoothDevice = leDeviceListAdapter?.getDevice(position) ?: return
        handler?.removeCallbacksAndMessages(null);

        scanLeDevice(false) // stop scanning

        // start a foreground service to monitor the CO2 values
        val startIntent = Intent(applicationContext, CO2Service::class.java)
        startIntent.action = CO2_ACTION_START_SERVICE
        startIntent.putExtra(CO2Service.EXTRAS_DEVICE_NAME, device.name)
        startIntent.putExtra(CO2Service.EXTRAS_DEVICE_ADDRESS, device.address)
        startIntent.putExtra(CO2Service.EXTRAS_THRESHOLD_CO2, sharedPref!!.getInt(
            getString(R.string.alert_key), getString(R.string.alert_default).toInt()))
        startIntent.putExtra(CO2Service.EXTRAS_USE_WAKELOCK, sharedPref!!.getBoolean(
            getString(R.string.wakelock_key), false))
        startIntent.putExtra(CO2Service.EXTRAS_ALERT_ONCE_CO2, sharedPref!!.getBoolean(
            getString(R.string.alert_once_key), true))

        startService(startIntent)
        finish()
    }


    private fun scanLeDevice(enable: Boolean) {
        Log.d(TAG, "scanLeDevice($enable)")
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            handler!!.postDelayed(Runnable {
                scanning = false
                actionBar.setTitle(R.string.title_devices)
                bluetoothAdapter?.stopLeScan(leScanCallback)
                invalidateOptionsMenu()
                if (leDeviceListAdapter!!.isEmpty) {
                    Toast.makeText(this, R.string.error_no_device_found, Toast.LENGTH_SHORT).show()
                }
            }, SCAN_PERIOD)
            scanning = true
            bluetoothAdapter?.startLeScan(leScanCallback)
            actionBar.setTitle(R.string.title_devices_scanning)
        } else {
            scanning = false
            bluetoothAdapter?.stopLeScan(leScanCallback)
            actionBar.setTitle(R.string.title_devices)
        }

        invalidateOptionsMenu()
    }

    // Device scan callback.
    private val leScanCallback =
        LeScanCallback { device, rssi, scanRecord ->
            runOnUiThread {
                // populate the list with devices that have a name
                if (device.name != null && device.name.isNotEmpty()) {
                    Log.v(TAG, "leScanCallback $device")

                    leDeviceListAdapter?.addDevice(device)
                    leDeviceListAdapter?.notifyDataSetChanged()
                }
            }
        }

    // Adapter for holding devices found through scanning.
    private class LeDeviceListAdapter(CO2Activity: CO2Activity) : BaseAdapter() {
        private val leDevices: ArrayList<BluetoothDevice> = ArrayList()
        private val inflater: LayoutInflater = CO2Activity.layoutInflater

        fun addDevice(device: BluetoothDevice) {
            if (!leDevices.contains(device)) {
                leDevices.add(device)
            }
        }

        fun getDevice(position: Int): BluetoothDevice {
            return leDevices[position]
        }

        fun clear() {
            leDevices.clear()
        }

        override fun getCount(): Int {
            return leDevices.size
        }

        override fun getItem(i: Int): Any {
            return leDevices[i]
        }

        override fun getItemId(i: Int): Long {
            return i.toLong()
        }

        override fun getView(i: Int, v: View?, viewGroup: ViewGroup): View {
            var view = v
            val viewHolder: ViewHolder
            // General ListView optimization code.
            if (view == null) {
                view = inflater.inflate(R.layout.listitem_device, null)
                viewHolder = ViewHolder()
                viewHolder.deviceAddress = view.findViewById<View>(R.id.device_address) as TextView
                viewHolder.deviceName = view.findViewById<View>(R.id.device_name) as TextView
                view.tag = viewHolder
            } else {
                viewHolder = view.tag as ViewHolder
            }
            val device = leDevices[i]
            val deviceName = device.name
            if (deviceName != null && deviceName.isNotEmpty()) viewHolder.deviceName!!.text =
                deviceName else viewHolder.deviceName?.setText(R.string.unknown_device)
            viewHolder.deviceAddress!!.text = device.address
            return view!!
        }

    }

    internal class ViewHolder {
        var deviceName: TextView? = null
        var deviceAddress: TextView? = null
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        // trigger the service to update the changed values
        // the service will not do much if it is not already running.
        val startIntent = Intent(applicationContext, CO2Service::class.java)
        startIntent.action = CO2_ACTION_START_SERVICE
        if (key == getString(R.string.alert_key))
            startIntent.putExtra(CO2Service.EXTRAS_THRESHOLD_CO2, sharedPreferences!!.getInt(
                getString(R.string.alert_key), getString(R.string.alert_default).toInt()
            ))
        if (key == getString(R.string.wakelock_key))
            startIntent.putExtra(CO2Service.EXTRAS_USE_WAKELOCK, sharedPreferences!!.getBoolean(
                getString(R.string.wakelock_key), false)
            )
        if (key == getString(R.string.alert_once_key))
            startIntent.putExtra(CO2Service.EXTRAS_ALERT_ONCE_CO2, sharedPreferences!!.getBoolean(
                getString(R.string.alert_once_key), true)
            )

        startService(startIntent)
    }
}