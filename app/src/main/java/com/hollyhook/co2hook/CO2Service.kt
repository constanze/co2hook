package com.hollyhook.co2hook

import android.app.*
import android.bluetooth.*
import android.bluetooth.BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.Uri
import android.os.*
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat.*
import androidx.core.app.NotificationManagerCompat
import java.util.*
import kotlin.math.max
import kotlin.math.min


class CO2Service : Service() {
    private var firstDataReceived: Boolean = false

    private val TAG = "com.hollyhook.CO2Srv"

    // for notifications, one has sound
    private var channelLoId: String = ""
    private var channelHiId: String = ""

    private var co2 = 0 // last measured value

    private var settingCo2threshold = 0
    private var settingCo2alertOnce = false
    private var settingWakelock = false

    private var bluetoothManager: BluetoothManager? = null
    private var bluetoothAdapter: BluetoothAdapter? = null
    private var bluetoothDeviceAddress: String? = null
    private var bluetoothGatt: BluetoothGatt? = null

    private val STATE_DISCONNECTED = 0
    private val STATE_CONNECTING = 1
    private val STATE_CONNECTED = 2

    private var connectionState = STATE_DISCONNECTED
    private var handlerPoll: Handler? = null
    private var handlerGoneNotify: Handler? = null
    private var handlerTimeout: Handler? = null

    private val AR4_SERVICE = "f0cd1400-95da-4f4b-9ac8-aa55d312af0c"
    private val AR4_READ_CURRENT_READINGS = "f0cd1503-95da-4f4b-9ac8-aa55d312af0c"
    private val AR4_READ_CURRENT_READINGS_DETAILS = "f0cd3001-95da-4f4b-9ac8-aa55d312af0c"
    // val AR4_INTERVAL = "f0cd2002-95da-4f4b-9ac8-aa55d312af0c"

    var wakelock: PowerManager.WakeLock? = null
    var wlCounter = 0

    override fun onCreate() {
        handlerPoll = Handler()
        handlerGoneNotify = Handler()
        handlerTimeout = Handler()

        Log.d(TAG, "onCreate")
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand " + intent.action)
        if (intent.action == CO2_ACTION_START_SERVICE) {
            if (intent.hasExtra(EXTRAS_USE_WAKELOCK)) {
                val newVal = intent.extras!!.getBoolean(EXTRAS_USE_WAKELOCK)
                Log.d(TAG, "EXTRAS_USE_WAKELOCK $newVal")
                if (settingWakelock && wakelock != null && !newVal) {
                    wakelock?.release() // released, this wakelock is from a previous run
                    wlCounter--
                    wakelock = null
                }
                settingWakelock = newVal
            }
            if (intent.hasExtra(EXTRAS_THRESHOLD_CO2)) {
                settingCo2threshold = intent.extras!!.getInt(EXTRAS_THRESHOLD_CO2)
                Log.d(TAG, "EXTRAS_THRESHOLD_CO2 $settingCo2threshold")
            }
            if (intent.hasExtra(EXTRAS_ALERT_ONCE_CO2)) {
                settingCo2alertOnce = intent.extras!!.getBoolean(EXTRAS_ALERT_ONCE_CO2)
                Log.d(TAG, "EXTRAS_ALERT_ONCE_CO2 $settingCo2alertOnce")
            }
            // (re-)start only if device name is given
            if (intent.hasExtra(EXTRAS_DEVICE_NAME) && intent.hasExtra(EXTRAS_DEVICE_ADDRESS)) {
                val devName = intent.extras!!.getString(EXTRAS_DEVICE_NAME)!!
                val devAddress = intent.extras!!.getString(EXTRAS_DEVICE_ADDRESS)!!

                Log.d(TAG, "threshold for alert is $settingCo2threshold, use wake lock $settingWakelock")

                // we were already running, so have to remove notifications, etc.
                if (firstDataReceived) {
                    stopMyService()
                }

                if (initialize()) {
                    Log.d(TAG, "start service for device $devName ($devAddress)")
                    startServiceWithNotification(devName)
                    // connect to the GATT server on the device
                    connect(devAddress)
                } else stopMyService()
            }
        } else stopMyService()
        return START_STICKY
    }

    // In case the service is deleted or crashes some how
    override fun onDestroy() {
        Log.d(TAG, "onDestroy wakelock counter: $wlCounter")
        if (BuildConfig.DEBUG && wlCounter != 0) {
            error("Assertion failed")
        }
        isServiceRunning = false
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? {
        Log.d(TAG, "onBind")

        // Used only in case of bound services.
        return null
    }

    private fun startServiceWithNotification(devName: String) {
        if (isServiceRunning) return
        isServiceRunning = true

        channelLoId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("$TAG.low", false)
            } else {
                // If earlier version channels are not available
                ""
            }
        channelHiId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("$TAG.high",true)
            } else { "" }

        val stat = String.format(resources.getString(R.string.status_connecting_to), devName)
        startForeground(PHONE_NOTIFICATION_ID, buildPhoneNotification(stat, null, null,  false))
    }

    /**
     * returns channel ID. Makes some noise if the channel has high prio
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, hasPrio: Boolean): String {
        val channelName = if (hasPrio) getString(R.string.channel_name_high)
                                            else getString(R.string.channel_name_low)
        val importance = if (hasPrio) NotificationManager.IMPORTANCE_DEFAULT
                                            else NotificationManager.IMPORTANCE_LOW
        val visibility = if (hasPrio) Notification.VISIBILITY_PUBLIC
                                            else Notification.VISIBILITY_PRIVATE

        val chan = NotificationChannel(
            channelId,
            channelName,
            importance
        )

        chan.lockscreenVisibility = visibility
        chan.enableVibration(hasPrio)

        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    private fun buildPhoneNotification(
        device: String,
        text: String?,
        bmp: Bitmap?,
        hasPrio: Boolean
    ) : Notification {

        val intent = Intent(this, CO2Service::class.java)
        intent.action = CO2_ACTION_CLOSE_SERVICE // will end myself

        val closeIntent = PendingIntent.getService(
            this,
            1, intent, PendingIntent.FLAG_UPDATE_CURRENT
        )

        // intent for when notification is tapped, restart activity
        val notificationIntent = Intent(applicationContext, CO2Activity::class.java)
        notificationIntent.action = "CO2.action.main" // A string containing the action name
        notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val contentPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

        val chan = if (hasPrio) channelHiId else channelLoId
        val p: Int        = if (hasPrio) PRIORITY_HIGH else PRIORITY_LOW // old style

        val n = Builder(this, chan)
            .setContentTitle(text)
            .setContentText(device)
            .setSmallIcon(R.drawable.hook24)
            .setContentIntent(contentPendingIntent)
            .setPriority(p)
            .setLargeIcon(bmp)
            .setOngoing(true) // which is the default for a foreground service
            .addAction(R.drawable.close_24px, getString(R.string.action_quit), closeIntent)

        if (hasPrio) {
            val uri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            n.setSound(uri).setVibrate(longArrayOf(1000, 1000, 1000))
        }

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
            n.setGroup("WEAR_GROUP")
             .setGroupSummary(false) // makes it phone only

        return n.build()
    }


    /**
     * build a notification for wear os.
     * kitkat (android v19) and jellybean (v18) have no wear support
     */
    private fun buildWearNotification(
        device: String,
        text: String?,
        bmp: Bitmap,
        hasPrio: Boolean) : Notification? {

        return if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            val chan = if (hasPrio) channelHiId else channelLoId
            val p: Int        = if (hasPrio) PRIORITY_HIGH else PRIORITY_LOW
            Builder(this, chan)
                .setContentTitle(text)
                .setContentText(device)
                .setGroup("WEAR_GROUP")
                .setGroupSummary(true)
                .setAutoCancel(true) // automatically removes the notification when the user taps it.
                .setSmallIcon(R.drawable.hook24)
                .setPriority(p)
                .setLargeIcon(bmp)
                .setStyle(BigPictureStyle().bigPicture(bmp))
                .build()

        } else null
    }


    // this is for notifications which show the status of the connection
    // removes the wear notification
    fun phoneNotificationUpdate(text: String, title: String?) {
        val notification = buildPhoneNotification(text, title, null, false)
        NotificationManagerCompat.from(this).apply {
            notify(PHONE_NOTIFICATION_ID, notification)
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
                cancel(WEAR_NOTIFICATION_ID)
        }
    }

    fun notificationUpdate(device: String, ppm: Int, temp: Float, hum: Int) {
        val tempStr = String.format("%.1f", temp)
        val humStr = "$hum%" //// \uD83D\uDCA7"
        val text = String.format(resources.getString(R.string.notification_meas),
            ppm, tempStr, humStr)

        // large icon depends on ppm, and on threshold
        var bmid = R.mipmap.large_blue
        if (ppm > 1400) bmid = R.mipmap.large_red
        else if (ppm > 1000) bmid = R.mipmap.large_yellow
        if (ppm > settingCo2threshold && settingCo2threshold != 0)
            bmid = R.mipmap.large_red

        val bmp: Bitmap = BitmapFactory.decodeResource(resources, bmid)

        var prio = false
        if (ppm > settingCo2threshold && settingCo2threshold != 0) {
            if (settingCo2alertOnce && co2 < settingCo2threshold) {
                prio = true
                Log.i(TAG, "CO2 threshold exceeded, alert once")
            } else if (!settingCo2alertOnce) {
                prio = true
                Log.i(TAG, "CO2 threshold exceeded")
            }
        }
        co2 = ppm

        val pn = buildPhoneNotification(device, text, bmp, prio)
        val wn = buildWearNotification(device, text, bmp, prio)

        NotificationManagerCompat.from(this).apply {
            notify(PHONE_NOTIFICATION_ID, pn)
            if (wn != null) {
                notify(WEAR_NOTIFICATION_ID, wn)
            }
        }
    }


    private fun stopMyService() {
        handlerPoll?.removeCallbacksAndMessages(null)
        handlerGoneNotify?.removeCallbacksAndMessages(null)
        handlerTimeout?.removeCallbacksAndMessages(null)
        if (wakelock != null) {
            Log.d(TAG, "release wakelock")
            wakelock?.release()
            wlCounter--
            wakelock = null
        }
        firstDataReceived = false

        // remove wear notification. the other one will be removed automatically
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
            NotificationManagerCompat.from(this).apply {
                cancel(WEAR_NOTIFICATION_ID)
            }
        // end connection with BT device
        if (connectionState != STATE_DISCONNECTED)
            disconnect()
        close()

        stopForeground(true)
        stopSelf()
        isServiceRunning = false
    }


    /**
     * Initializes a reference to the local Bluetooth adapter.
     * this should work, since the activity did the same checks.
     * @return Return true if the initialization is successful.
     */
    private fun initialize(): Boolean {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (bluetoothManager == null) {
            bluetoothManager = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
            if (bluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.")
                return false
            }
        }
        bluetoothAdapter = bluetoothManager!!.adapter
        if (bluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.")
            return false
        }
        return true
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * `BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)`
     * callback.
     */
    private fun connect(address: String?): Boolean {
        if (bluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.")
            return false
        }

        // Previously connected device.  Try to reconnect.
        if (bluetoothDeviceAddress != null && address == bluetoothDeviceAddress && bluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.")

            // after 1 sec, clear text of the notification,
            // which could be an old CO2 value
            handlerGoneNotify!!.postDelayed({
                val s1 = String.format(getString(R.string.notification_reconnect),
                    bluetoothGatt!!.device.name)
                val s2 = String.format(getString(R.string.notification_reconnect_title),
                    bluetoothGatt!!.device.name)

                phoneNotificationUpdate(s1, s2)
            }, 1000)

            // This method is used to re-connect to a remote device after
            // the connection has been dropped. If the device is not in range,
            // the re-connection will be triggered once the device is back in range.

            return if (bluetoothGatt!!.connect()) {
                connectionState = STATE_CONNECTING
                true
            } else {
                Log.w(TAG, "re-connect to " + bluetoothGatt!!.device + " failed . try again?")
                false
            }
        }
        val device: BluetoothDevice = bluetoothAdapter!!.getRemoteDevice(address)

        // new device
        // but in case this is not answering at all, warn and exit
        handlerTimeout?.postDelayed( {
            Log.w(TAG, "timeout, giving up")
            val cs: CharSequence = String.format(
                getString(R.string.toast_connection_failed),
                device.name
            )
            Toast.makeText(
                applicationContext, cs, Toast.LENGTH_LONG
            ).show()
            stopMyService()
        }, 10*1000)

        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.

        bluetoothGatt = device.connectGatt(this, false, gattCallback)
        Log.d(TAG, "Trying to create a new connection to $address")
        bluetoothDeviceAddress = address
        connectionState = STATE_CONNECTING
        return true
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * `BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)`
     * callback.
     */
    fun disconnect() {
        if (bluetoothAdapter == null || bluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized")
            return
        }
        bluetoothGatt!!.disconnect()
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    private fun close() {
        if (bluetoothGatt == null) {
            return
        }
        bluetoothGatt!!.close()
        bluetoothGatt = null
    }


    companion object {
        const val PHONE_NOTIFICATION_ID = 10627
        const val WEAR_NOTIFICATION_ID = 10626
        var isServiceRunning = false
        const val EXTRAS_DEVICE_NAME =    "DEVICE_NAME"
        const val EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS"
        const val EXTRAS_THRESHOLD_CO2  = "THRESHOLD_CO2"
        const val EXTRAS_ALERT_ONCE_CO2 = "ALERT_ONCE_CO2"
        const val EXTRAS_USE_WAKELOCK   = "USE_WAKELOCK"

        const val CO2_ACTION_START_SERVICE = "CO2_ACTION_START_SERVICE"
        const val CO2_ACTION_CLOSE_SERVICE = "CO2_ACTION_CLOSE_SERVICE"
    }


    //--- GattCallback --------------------------------------------------------

    /**
     * Implements callback methods for GATT events that the app cares about.  For example,
     * connection change and services discovered. Changes are reported to the notification
     */
    private val gattCallback: BluetoothGattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {

            val newStr: String = when (newState) {
                BluetoothProfile.STATE_DISCONNECTED -> "disconnected"
                BluetoothProfile.STATE_CONNECTING -> "connecting"
                BluetoothProfile.STATE_CONNECTED -> "connected"
                BluetoothProfile.STATE_DISCONNECTING -> "disconnecting"
                else -> "unknown"
            }
            Log.d(TAG, "onConnectionStateChange to $newStr")

            handlerTimeout?.removeCallbacksAndMessages(null)

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                connectionState = STATE_CONNECTED
                phoneNotificationUpdate("Connecting to " + gatt.device.name, null)

                // Attempts to discover services after successful connection.
                Log.d(
                    TAG, "Attempting to start service discovery:" +
                            bluetoothGatt!!.discoverServices()
                )

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                connectionState = STATE_DISCONNECTED
                Log.d(TAG, "Disconnected from GATT server.")
                if (!firstDataReceived) {
                    // this did not work, warn and exit
                    val handler = Handler(Looper.getMainLooper())
                    handler.post {
                        val cs: CharSequence = String.format(
                            getString(R.string.toast_connection_failed),
                            bluetoothGatt?.device?.name
                        )
                        Toast.makeText(
                            applicationContext, cs, Toast.LENGTH_LONG
                        ).show()
                    }
                    stopMyService()
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            Log.d(TAG, "onServicesDiscovered $status")

            if (status != BluetoothGatt.GATT_SUCCESS) return

            Log.d(TAG, "onServicesDiscovered success")
            phoneNotificationUpdate(" .. GATT services discovered", null)
            var found = false
            for (bgs in gatt.services) {
                if (bgs.uuid == UUID.fromString(AR4_SERVICE)) {
                    Log.d(TAG, "AR4 service discovered ")
                    found = true

                    // kill the ... is gone ... notification, if it is still in the air
                    handlerGoneNotify?.removeCallbacksAndMessages(null)
                    phoneNotificationUpdate(" .. AR4 service connected", null)

                    var cRead1: BluetoothGattCharacteristic? = null
                    var cRead2: BluetoothGattCharacteristic? = null
                    var cRead: BluetoothGattCharacteristic? = null

                    for (c in bgs.characteristics) {
                        if (c.uuid == UUID.fromString(AR4_READ_CURRENT_READINGS)) {
                            Log.d(TAG, "AR4 read current readings characteristics found")
                            cRead1 = c
                        } else if (c.uuid == UUID.fromString(AR4_READ_CURRENT_READINGS_DETAILS)) {
                            Log.d(TAG, "AR4 read current readings details character. found")
                            cRead2 = c
                        }
                    }

                    when {
                        cRead2 != null -> cRead = cRead2 // details preferred
                        cRead1 != null -> cRead = cRead1
                        else -> {
                            Log.e(TAG, "AR4 characteristics are not compatible")
                            // TOAST whatever, finish
                            stopMyService()
                        }
                    }

                    // we found the right server, next is to read it ...
                    if (!firstDataReceived) {
                        // the activity is gone, explain where it is
                        val handler = Handler(Looper.getMainLooper())
                        handler.post {
                            val cs: CharSequence = String.format(
                                getString(R.string.toast_connected_to),
                                bluetoothGatt?.device?.name
                            )
                            Toast.makeText(
                                applicationContext, cs, Toast.LENGTH_LONG
                            ).show()
                        }
                        // since all seem to be working smoothly, we make a partial wake log to
                        // prevent the CPU going to sleep. This is a bad thing for
                        // memory and power consumption, but otherwise the timing when
                        // the handler is called gets too unreliable
                        if (settingWakelock && wakelock == null) {
                            val pm: PowerManager = getSystemService(Context.POWER_SERVICE) as PowerManager
                            wakelock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "$TAG:pwl")
                            Log.d(TAG, "requesting partial wake lock")
                            wakelock?.acquire() // released on exiting the service
                            wlCounter++
                        }

                        firstDataReceived = true
                    }
                    gatt.readCharacteristic(cRead)
                }
            }
            if (!found) {
                // this is the wrong device
                val handler = Handler(Looper.getMainLooper())
                handler.post {
                    val cs: CharSequence = String.format(
                        getString(R.string.toast_service_not_found),
                        bluetoothGatt?.device?.name
                    )
                    Toast.makeText(
                        applicationContext, cs, Toast.LENGTH_LONG
                    ).show()
                }
                stopMyService()
            }
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int
        ) {
            Log.d(TAG, "onCharacteristicRead $status")
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (characteristic.uuid == UUID.fromString(AR4_READ_CURRENT_READINGS_DETAILS)
                    || characteristic.uuid == UUID.fromString(AR4_READ_CURRENT_READINGS)) {

                    val co2: Int =
                        characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0)
                    val temp: Float =
                        characteristic.getIntValue(
                            BluetoothGattCharacteristic.FORMAT_UINT16,
                            2
                        ) / 20f
                    val press: Float =
                        characteristic.getIntValue(
                            BluetoothGattCharacteristic.FORMAT_UINT16,
                            4
                        ) / 10f
                    val humidity: Int =
                        characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 6)
                    val battery: Int =
                        characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 7)

                    val res = String.format(
                        "%d ppm, %.2f°, pressure %.1f, humidity %d, battery %d",
                        co2, temp, press, humidity, battery
                    )
                    Log.i(TAG, String.format("Received current readings: %s", res))

                    notificationUpdate(gatt.device.name, co2, temp, humidity)

                    var nextRound = 5*60*1000L
                    if (characteristic.uuid == UUID.fromString(AR4_READ_CURRENT_READINGS_DETAILS)) {
                        val interval: Int =
                            characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 9)
                        val ago: Int =
                            characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 11)
                        Log.i(TAG, String.format("interval %d, ago %d", interval, ago))
                        //nextRound = min(60000, (interval - ago + 2)*1000L) for testing

                        nextRound = min(10*
                                60*1000, max(3000, (interval - ago + 2) * 1000L))
                    }

                    // end the connection to the GATT server, and open it again later
                    disconnect()

                    if (wakelock != null && wakelock!!.isHeld)
                        Log.d(TAG, "wake lock still held $wlCounter")
                    else
                        Log.d(TAG, "wake lock not active $wlCounter")

                    handlerPoll!!.postDelayed({
                        connect(gatt.device.address)
                    }, nextRound)


                }
            } else if (status == GATT_INSUFFICIENT_AUTHENTICATION) {
                Log.e(TAG, "Authentification failed")
                phoneNotificationUpdate("Authentication required", "Enter pin to connect")
            } else {
                phoneNotificationUpdate("Failed to receive CO2 measurements", gatt.device.name)
            }
        }
    }

}