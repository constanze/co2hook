package com.hollyhook.co2hook

import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.TextView

/**
 * Show info about anton anton wtf is anton?
 */
class HelpActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        // Be sure to call the super class.
        super.onCreate(savedInstanceState)

        // See assets/res/any/layout/dialog_activity.xml for this
        // view layout definition, which is being set here as
        // the content of our screen.
        setContentView(R.layout.activity_help)

        // get version number
        val versionNumber: String
        versionNumber = try {
            val pkg = packageName
            packageManager.getPackageInfo(pkg, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            "?"
        }

        // fill in version number
        val helptext = getString(R.string.help_text)
        val tv = findViewById<View>(R.id.help_text) as TextView
        tv.text = Html.fromHtml(
            "<h2>co2Hook</h2><div align=\"center\"><small>version "
                    + versionNumber
                    + "</small></div>"
                    + helptext
        )
        val t2 = findViewById<View>(R.id.contributions) as TextView
        t2.movementMethod = LinkMovementMethod.getInstance()
    }
}