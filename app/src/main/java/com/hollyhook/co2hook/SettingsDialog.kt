package com.hollyhook.co2hook

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast


class SettingsDialog : DialogFragment() {
  private val TAG = "SettingsDialog"

  // Use this instance of the interface to get members of the parent
  private lateinit var parent: CO2Activity

  override fun onAttach(activity: Activity) {
    super.onAttach(activity)
    Log.v(TAG, "onAttach")
    try {
      parent = activity as CO2Activity
    } catch (e: ClassCastException) {
        Log.e(TAG, "invalid parent class")
    }
  }


  override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
    Log.v(TAG, "onCreateDialog")
    val factory = LayoutInflater.from(parent)
    // add a custom layout to an AlertDialog
    val settingsView: View = factory.inflate(R.layout.dialog_settings, null)

    val oldWL: Boolean? = parent.sharedPref!!.getBoolean(
      getString(R.string.wakelock_key), false)
    var wakeLock: Boolean = oldWL ?: false

    val oldThreshold: Int? = parent.sharedPref!!.getInt(
      getString(R.string.alert_key),
      getString(R.string.alert_default).toInt()
    )
    var threshold: Int = oldThreshold ?: 0
    val oldAO: Boolean? = parent.sharedPref!!.getBoolean(
      getString(R.string.alert_once_key), false)
    var alertOnce: Boolean = oldAO ?: false

    (settingsView.findViewById(R.id.alert_edit) as EditText).setText(threshold.toString())
    (settingsView.findViewById(R.id.wakelock) as CheckBox).isChecked = wakeLock
    (settingsView.findViewById(R.id.alert_once) as CheckBox).isChecked = alertOnce

    return AlertDialog.Builder(parent)
      .setView(settingsView)
      .setPositiveButton(R.string.alert_dialog_ok) { _, _ ->

        // ok button clicked
        wakeLock = (settingsView.findViewById(R.id.wakelock) as CheckBox).isChecked
        alertOnce = (settingsView.findViewById(R.id.alert_once) as CheckBox).isChecked

        val resThreshold: CharSequence =
          (settingsView.findViewById<View>(R.id.alert_edit) as TextView).text
        threshold = try {
          resThreshold.toString().toInt()
        } catch (e: java.lang.Exception) {
          // don't change the threshold
          val toast = Toast.makeText(parent, R.string.error_threshold_value, Toast.LENGTH_LONG)
          toast.show()
          oldThreshold?:0
        }
        // save values
        val editor: SharedPreferences.Editor = parent.sharedPref!!.edit()
        editor.putInt(getString(R.string.alert_key), threshold)
        editor.putBoolean(getString(R.string.alert_once_key), alertOnce)
        editor.putBoolean(getString(R.string.wakelock_key), wakeLock)
        editor.apply()
      }
      .create()
  }
}