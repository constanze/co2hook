# co2Hook Android App

### Overview

An Anrdoid app to monitor CO2 measurements of an ARANET4 with your Android phone. Alert when CO2 gets to high.

![picture](doc/device-2020-10-25-135959.png)

### What it does

co2Hook connects to an ARANET4 device vial Bluetooth LE,
gets it's current measurements and shows them as an Android notification. For CO2 measurements,
you can configure a threshold. If the measurement is above that threshold, co2Hook will
notify with sound and vibration. Android Wear notifications are supported as well.
You can get an ARANET4 device here: https://aranet4.com/. Feel free to raise a pull request
if you want co2Hook to support other CO2 measurement devices.

Supported are devices back to Andoid Lollipop (v18).

### Download

Get the official apk via Google Play: https://play.google.com/store/apps/details?id=com.hollyhook.co2hook

### License

co2Hook is licensed under the [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0). No warranty is given.

